import { combineReducers } from 'redux';
import productReducer from './product/reducers/productReducer';
import profileReducer from "./product/reducers/profileReducer";

export default combineReducers({
  product: productReducer,
  profile: profileReducer
});