import React from 'react';
import Profile from "../product/pages/Profile";
import {Link, Route} from "react-router-dom";

const Home = () => {
  return (
    <div>
      <h3>This is a beautiful home page.</h3>
      <ul>
        <li>
          <Link to='/product-details'>Go to Product Details Page</Link>
        </li>
      </ul>
      <h2>User Profile:</h2>
      <ul>
        <li><Link to="/user-profiles/1">User Profile 1</Link></li>
        <li><Link to="/user-profiles/2">User Profile 2</Link></li>
      </ul>

    </div>
  )
};

export default Home;