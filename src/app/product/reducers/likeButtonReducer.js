const initState = {
  likeStatus: {}
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_LIKE_BUTTON_STATUS':
      return {
        ...state,
        likeStatus: action.likeStatus
      };

    default:
      return state
  }
};
