const initState = {
  profileDetails: {}
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_PROFILE_DETAILS':
      return {
        ...state,
        profileDetails: action.profileDetails
      };

    default:
      return state
  }
};