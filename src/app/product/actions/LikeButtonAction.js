export const getLikeButtonStatus = () => (dispatch) => {
  dispatch({
    type: 'GET_LIKE_BUTTON_STATUS',
    likeStatus: {status: true}
  });
};