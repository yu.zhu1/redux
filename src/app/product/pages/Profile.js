import React, {Component} from 'react';
import {connect} from "react-redux";
import {getProfileDetails} from '../actions/profileActions';
import {bindActionCreators} from "redux";
import {getLikeButtonStatus} from "../actions/LikeButtonAction";

class Profile extends Component {

  constructor(props, context) {
    super(props, context);
    this.clickLike = this.clickLike.bind(this);
  }

  componentDidMount() {
    this.props.getProfileDetails();
  }

  render() {
    const {userName, gender, descripts} = this.props.profileDetails;
    const {status} = this.props.likeButtonStatus;
    return (
      <div>
        <h1>User Profile</h1>
        <ul>
          <li>User name: {userName}</li>
          <li>Gender: {gender}</li>
          <li>Description: {descripts}</li>
        </ul>
        <button disabled={status} onClick={this.clickLike}>Like</button>
      </div>
    );
  }

  clickLike() {
    let data = {userProfileId:1, userName: 'anonymous user'};
    fetch("http://localhost:8080/api/like", {method:'POST',headers: {'Content-Type': 'application/json'}, body: JSON.stringify(data)})
      .then(response => response.json())
      .then(result => {
        console.log(result)
      });
    this.props.getLikeButtonStatus();
  }


}

const mapStateToProps = state => ({
  profileDetails: state.profile.profileDetails,
  likeButtonStatus: state.profile.likeStatus
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getProfileDetails,
  getLikeButtonStatus
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Profile);