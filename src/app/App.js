import React from 'react';
import ProductDetails from "./product/pages/ProductDetails";
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from "./home/Home";
import Profile from "./product/pages/Profile";

const App = () => {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/product-details" component={ProductDetails} />
          <Route exact path="/" component={Home}/>
          <Route path="/user-profiles/:id" component={Profile} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;